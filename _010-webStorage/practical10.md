# webStorage API

Storing data locally across browser sessions with webStorage

Make sure to check the documentation online:

* https://www.w3schools.com/JS/js_api_web_storage.asp
* https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API

## Practical 10

Study the example code, specifically observing the localStorage setting and getting, as well as how you can hide and show a div (or other section of html) programatically in Javascript.

Add the following scenario to your existing code-base from practical 9:

- Make the canvas invisible for the users first visit
- Make a simple form appear that asks the user to input their name/gamertag and any other info you might need (look at your own code from practical 3)
- Store the inputted values into localStorage
- Make the form dissapear (using css) but NOT by redirecting the user via the `action=""` attribute of your form
- Make the canvas reappear and let the user play the game. Use localStorage to track their progress (achieving levels, accumulated score, whatever might be relevant to your game)
- Write code that enables the player to close the game tab, re-open the game and be given the option to restore their stored settings, name, score, etc.

