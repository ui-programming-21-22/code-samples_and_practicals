# UI Programming module 2021 - 2022

* </01-web-basics/>
* </02-git-version-control/>
* </03-web-forms_data-reception/>
* </04-canvas/>
* </05A-scope-var-lets-conts/>
* </05B-audio_and_canvas-transformations/>
* </06-event-listeners/>
* </07-buttons-and-dpads/>
* </08-animations/>
* </09-gameloops/>
* </010-webStorage/>
* </011-gitlab-pages/>
* </012-PWA/>
* </013-project/>

## CA (continuous assessment / practicals) submission spreadsheet

https://instituteoftechnol663-my.sharepoint.com/:x:/g/personal/oneillco_itcarlow_ie/EfT4SyhR7wVBq8V3RHCk46UBf9nQROVuCUF7NeOyuJMEwQ?e=YWT2tB

## grading scheme

#### 0 – 35% Basic

You will obtain a grade in this range if you:

- deliver partial amounts of the practical
- push a repo to gitlab
- post a link to the CA spreadsheet

#### 35 - 75% Intermediate

You will obtain a grade in this range if you:

- meet all the spects in the briefing
- display a genuine understanding of the themes and practices of the practical
- surpass the practical requirements and display an ability to research and add to the requirements


#### 75 - 100 Advanced

You will obtain a grade in this range if you:

- surpass the practical by demonstrating particular interest in the subjects by adding a lot to the brief, relevant content, advanced styling and genuine interest.

## List of short documents and practices

* flipping-spritesheets
* edge-detection
* collision-detection
* multi-key-input-registration

