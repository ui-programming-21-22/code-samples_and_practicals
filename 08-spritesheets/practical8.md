# Spritesheets practical 8

In this practical you are asked to:

1. build on your code from practical 7
2. include a spritesheet animation process (either from chapter 8.1 or 8.2, the latter being much more robust and flexible)
3. make one of your in-canvas characters use a spritesheet
4. make that spritesheet use a walk/run/movement animation active when gamerInput is registered (keydown event), and disable the animation (or revert to an idle animation) when gamerInput is stopped (keyup event).

