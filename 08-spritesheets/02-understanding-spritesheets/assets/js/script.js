const canvas = document.getElementById("the_canvas")
const context = canvas.getContext("2d");

let image = new Image();
image.src = "assets/img/Green-16x18-spritesheet.png";
image.onload = gameloop;

function gameloop(){

    // 0 REMEMBER DRAWIMAGE FUNCTION
    // important to refer to docs here https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage
    // context.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
    
    // 1 DRAW ONE IMAGE FROM THE SPRITE USING sWidth and sHeight
    // context.drawImage(image, 0, 0, 16, 18, 0, 0, 16, 18);
    
    // 2 SCALE IT UP
    const scale = 8;
    //context.drawImage(imageFile, clippingXpos, clippingYpos, widthOfClip, heightOfClip, XpositionInCanvas, YpositionInCanvas, drawnWidth, drawnHeight);
    // context.drawImage(image, 0, 0, 16, 18, 0, 0, 16 * scale, 18 * scale);

    // 3 LETS DRAW TWO OTHER IMAGES FROM THE SPRITE BY CHANGING SX AND SY, PLUS MOVING DX SO THEY DONT OVERLAP
    // context.drawImage(image, 0, 0, 16, 18, 0, 0, 16 * scale, 18 * scale);
    // context.drawImage(image, 16, 0, 16, 18, 32, 0, 16 * scale, 18 * scale);
    // context.drawImage(image, 32, 0, 16, 18, 64, 0, 16 * scale, 18 * scale);
    
    // 4 CREATING A FEW VARIABLES TO SIMPLIFY READING
    const width = 16;
    const height = 18;
    const scaledWidth = scale * width;
    const scaledHeight = scale * height;

    // 5 USE OUR VARIABLES WITH THE DRAWIMAGE FUNCTION
    //context.drawImage(image, sx,        sy, sWidth, sHeight, dx,              dy, dWidth,      dHeight);
    //context.drawImage(image, 0,         0,  width,  height,  0,               0,  scaledWidth, scaledHeight);
    //context.drawImage(image, width,     0,  width,  height,  scaledWidth,     0,  scaledWidth, scaledHeight);
    //context.drawImage(image, width * 2, 0,  width,  height,  scaledWidth * 2, 0,  scaledWidth, scaledHeight);

    // 6 NOTICE THAT ONLY 4 ARGS ARE CHANGING: SX, SY, DX and DY, NO WIDTHS OR HEIGHTS CHANGE, SO LETS MAKE A FUNCTION TO FILL IN THE REPETITIONS FOR US.
    function drawFrame(frameX, frameY, canvasX, canvasY) {
        context.drawImage(image,
                      frameX * width, frameY * height, width, height,
                      canvasX, canvasY, scaledWidth, scaledHeight);
    }
    
    // 7 REPRODUCING STEP 5 WITH OUR NEW drawFrame FUNCTION LOOKS LIKE THE BELOW
    // drawFrame(0, 0, 0, 0);
    // drawFrame(1, 0, scaledWidth, 0);
    // drawFrame(0, 0, scaledWidth * 2, 0);
    // drawFrame(2, 0, scaledWidth * 3, 0);

    // 8 LOOKING AT THE SPRITESHEET, LETS CREATE A walkLoop ARRAY THAT WILL SELECT THE ORDER OF THE WALKING IMAGES
    // REMIND YOURSELF HOW ARRAYS WORK! arrayName[itemFromArray];
    // const walkLoop = [0, 1, 0, 2];
    // let currentLoopIndex = 0;

    // 9 NEXT LETS CREATE A STEP ANIMATION THAT WILL DRAW EACH SELECTED IMAGE TO CREATE THE WALK !! DONT FORGET THE clearRect TO WIPE THE CANVAS
    // function step() {
    //   ctx.clearRect(0, 0, canvas.width, canvas.height);
    //   drawFrame(walkLoop[currentLoopIndex], 0, 0, 0);
    //   currentLoopIndex++;
    //   if (currentLoopIndex >= walkCyle.length) {
    //     currentLoopIndex = 0;
    //   }
    //   window.requestAnimationFrame(step);
    // }

    // 10 AS YOU KNOW, requestAnimationFrame() FUNCTION RUNS AT 60FPS. THIS MEANS OUR CHARACTER IS MAKING ONE STEP EVERY 1/60 OF A SECOND
    // LETS SLOW THEM DOWN
    const walkLoop = [0, 1, 0, 2];
    let currentLoopIndex = 0;
    let frameCount = 0;

    // function step() {
    //     frameCount++;
    //     // NOW OUR CHARACTER SPEED IS CONTROLLED BY FRAMERATE
    //     if (frameCount < 15) {
    //       window.requestAnimationFrame(step);
    //       return;
    //     }
    //     frameCount = 0;
    //     context.clearRect(0, 0, canvas.width, canvas.height);
    //     drawFrame(walkLoop[currentLoopIndex], 0, 0, 0);
    //     currentLoopIndex++;
    //     if (currentLoopIndex >= walkLoop.length) {
    //       currentLoopIndex = 0;
    //     }
    //     window.requestAnimationFrame(step);
    //   }

    // window.requestAnimationFrame(step);

    // 11 LETS NOW USE THE OTHER IMAGES OF THE SPRITE TO MAKE THE CHARACTER WALK IN DIFFERENT DIRECTIONS
    // NOTICE THAT EACH ROW OF THE SPRITE IS DEDICATED TO ONE DIRECTION

    

    let currentDirection = 0;

    function step() {

      frameCount++;
      if (frameCount < 15) {
        window.requestAnimationFrame(step);
        return;
      }
      frameCount = 0;
      context.clearRect(0, 0, canvas.width, canvas.height);
      // ADDING IN OUR CURRENTDIRECTION VAR WHICH IS APPLIED TO OUR DRAWFRAME METHOD
      // drawFrame(frameX, frameY, canvasX, canvasY)
      // in drawFrame( frameX and frameY are multiplied by the width and height value, we sometimes call this a 'sprite atlas')
      drawFrame(walkLoop[currentLoopIndex], currentDirection, 0, 0);
      currentLoopIndex++;
      if (currentLoopIndex >= walkLoop.length) {
        currentLoopIndex = 0;
        console.log("changing direction to dir: " + currentDirection);
        currentDirection++;
      }
      if (currentDirection >= 4) {
        currentDirection = 0;
      }
      window.requestAnimationFrame(step);
    }
    
window.requestAnimationFrame(step);

      
    
}

