---
title: var, let, const
subtitle: Javascript variables and scope
author: Colm O'Neill
theme: metropolis
institute: Institute of Technology Carlow
date: Oct 2021
---


# Scope in JavaScript, vars, lets and consts

The latest version of Javascript was published in 2015 and is called ES6 or ECMAScript 6. It it important that we use the most up to date features in the language for our modern Progressive Web Apps to function well.

# Scope in JavaScript, vars, lets and consts

### references: 
* https://hashnode.com/post/scope-in-javascript
* https://www.freecodecamp.org/news/var-let-and-const-whats-the-difference/
* https://www.w3schools.com/Js/js_variables.asp

# Scope in JavaScript, vars, lets and consts

Javascript variables can be declared in three different ways:

* var 
* let
* const

# `var`

The latter two have only been available in the latest versions of Javascript and they got created because ultimately, var is considered to be too flexible. Example below:

# `var`

```
var greeter = "hey hi";
var times = 4;

if (times > 3) {
    var greeter = "say Hello instead"; 
}
    
console.log(greeter) // "say Hello instead"

```

# Scope in JavaScript, vars, lets and consts

> So, since times > 3 returns true, greeter is redefined  to "say Hello instead". While this is not a problem if you knowingly want greeter to be redefined, it becomes a problem when you do not realize that a variable greeter has already been defined before.

# Scope in JavaScript, vars, lets and consts

> If you have used greeter in other parts of your code, you might be surprised at the output you might get. This will likely cause a lot of bugs in your code. This is why let and const are necessary.

# Scope in JavaScript, vars, lets and consts{.standout}

**var is considered to be risky because it leaves itself open to be updated without you knowing about it**

# Scope in JavaScript, vars, lets and consts

`var` can still be used in JS because there are still many cases where it is appropriate, plus, if it was suddenly disabled or removed, loads of JS older than 2015 would just stop working. Retro-compatibility is important in programming languages. 

# `let` to the rescue

`let` is superior to var because it is what we call *block scoped*

a block is anything that is contained between `{}` so, conditionals, function declarations, ...

# `let`

example below:

```
let greeting = "say Hi";
let times = 4;

if (times > 3) {
    let hello = "say Hello instead";
    console.log(hello);// "say Hello instead"
}
console.log(hello) // hello is not defined

```

# `let`

`let` can be updated but not re-declared; example below:

```
// this will work and is considered legal
let trick = "using lets in js makes you look up to date";
trick = "using vars is ok too sometimes";

// this won't work and will throw an error
let trick = "using lets in js makes you look up to date";
let trick = "other things going on";
```

# `let`

but you can re-declare `let` in different scopes and you won't have any errors:

```
let greeting = "say Hi";
if (true) {
    let greeting = "say Hello instead";
    console.log(greeting); // "say Hello instead"
}
console.log(greeting); // "say Hi"
```

↑ the above is the demonstration of the advantage of `let` over `var` as when you use `let` you don't have to worry about variable existing already and possibly being re-declared. 

# `let`{.standout} 

`let` is block scoped or block contained

# const

## `const` is for constants

In addition to `let`, you can use a `const` for a variable that has a value that will not change.

const is also block scoped so it can be declared for specific use

# Hoisting


Hoisting is a JavaScript mechanism where variables and function declarations are moved to the top of their scope before code execution. This means that if we do this:

# Hoisting

```
console.log (greeter);
var greeter = "say hello"
```

it is interpreted as this:

```
var greeter;
console.log(greeter); // greeter is undefined
greeter = "say hello"
```


