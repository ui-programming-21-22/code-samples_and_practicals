# Practical 1

Create a web structure including at least:

- 1 html file
- linked CSS file
- linked js file

Your HTML file will make use of five HTML5 tags. Research online what new tags were added to HTML when version 5 came out and use them appropriately in your page. For example, you could use the `<nav>` tag to make a small menu, the `<aside>` tag for a side bar, etc.

Style each of the five HTML5 tags with CSS giving them unique visual attributes. Try to create web page that looks good, avoid clashing colours and jarring design choices.
