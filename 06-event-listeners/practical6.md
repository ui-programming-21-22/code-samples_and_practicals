# event Listeners 

List of HTML DOM Events

https://www.w3schools.com/jsref/dom_obj_event.asp

## Task 1:

Take your learnings from this chapter and apply them to moving objects in the canvas.

- use your keyboard arrow (or 'wasd') to move an image around the canvas, by updating the coordinates with the `context.drawImage()` function.
- configure an event listener for a sound to be triggered when you press the spacebar on you keyboard.