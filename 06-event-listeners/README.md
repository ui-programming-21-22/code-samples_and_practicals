# Chapter 6 JavaScript Events Overview

So far, we have used a small selection of different JS events, in different ways. In this chapter we will take a detailed look at most of the ways in which we can manage, listen and handle events and we will also figure out how keyboard / keypress events work and are encoded.

Here is a breakdown of the various methods we will cover:

* Inline Events (like we used to handle forms in chapter 3)
* usage of addEventListener
* usage of JavaScript Event Object
* bonus, if we have time: arrow function declaration

```
clickBox.addEventListener("click", function(ev){
    console.log(this);
});

// arrow function

clickBox.addEventListener("click", e => {
    console.log(this);
});
```


