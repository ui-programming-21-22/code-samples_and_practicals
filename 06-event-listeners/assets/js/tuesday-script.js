// console.log("hello from Tuesday script");

function changeColour() {
    const rectBox = document.querySelector('#rect');
    let randomColor = Math.floor(Math.random()*16777215).toString(16);
    randomColor = "#"+randomColor;
    rectBox.style.backgroundColor = randomColor;
    const extraText = document.querySelector(".colourCode");
    extraText.innerHTML = randomColor;
}

let clickBox = document.querySelector(".clickBox");
// Method 2.1
clickBox.addEventListener("click", function(event){
    console.log(event);
    let innerClickBox = document.querySelector(".innerClickBox");
    let contentInner = "click x: " + event.x + '<br>' + "click y: " + event.y + '<br>' + "AltKey: " + event.altKey + '<br>' + "CtrlKey: " + event.ctrlKey;
    innerClickBox.innerHTML = contentInner;
})

// method 2.2

clickBox.addEventListener("onmouseover", clickResponse2());

function clickResponse2(){
    console.log("that is correct Colm");
}

// method 3

//clickBox.onclick = clickResponse2

let character = document.querySelector(".character");
character.style.position = "relative";
character.style.left = "200px";
character.style.top = "200px";

window.addEventListener('keydown', input);

function input(event) {
    if (event.type === "keydown"){
        switch (event.keyCode){
            case 37: //left Arrow
                console.log("left arrow pressed");
                moveLeft();
                break;
            case 38: // Up arrow
                console.log("Up arrow pressed");
                moveUp();
                break;
            case 39: //right arrow
                console.log("right arrow pressed");
                moveRight();
                break;
            case 40: // down arrow
                console.log("down arrow pressed");
                moveDown();
                break;
            default:
                console.log("key input detected, but not connected to an action yet")
                
        } 
    }
}

function moveLeft(){
    character.style.left = parseInt(character.style.left) - 50 + "px"
}
function moveUp(){
    character.style.top = parseInt(character.style.top) - 50 + "px"
}
function moveRight(){
    character.style.left = parseInt(character.style.left) + 50 + "px"
}
function moveDown(){
    character.style.top = parseInt(character.style.top) + 50 + "px"
}