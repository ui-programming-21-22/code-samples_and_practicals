console.log("hello from script");

// so far we have viewed a few different ways to configure and handle event listeners:
// 1) from chapt 3:
// <form name="helloForm" onsubmit="validateForm()">
// </form>

function changeColour() {
    const rectBox = document.querySelector('#rect');
    let randomColor = Math.floor(Math.random()*16777215).toString(16); 
    //got that trick from https://css-tricks.com/snippets/javascript/random-hex-color/
    randomColor = "#"+randomColor;
    rectBox.style.backgroundColor = randomColor;
    let extraText = document.querySelector(".colourCode");
    extraText.innerHTML = randomColor;
}

// 2) from chapt 05B:

// btn.addEventListener("click", playAudio);

document.addEventListener("DOMContentLoaded", function(event) {
    let clickBox = document.querySelector('.clickBox');
    clickBox.addEventListener("click", function(e) {
        console.log(e);
        let innerClickBox = document.querySelector('.innerClickBox');
        innerClickBox.innerHTML = "click x: " + e.x + '<br>' + "click y: " + e.y + '<br>' + "altKey held: " + e.altKey + '<br>' + "ctrlKey held: " + e.ctrlKey;
    });
});

// document.addEventListener('keydown', logKey);

// function logKey(e) {
//     console.log('event object: ');
//     console.log(e);
//     console.log('key pressed: ' + e.key);
//     console.log('key.code: ' + e.keyCode);
// }

// keydown vs keyup
// note the order of the responses here, even if the keyup event is placed first in the code, the keydown event gets called first.

// document.addEventListener('keyup', function(event){
//     console.log("key up event");
//     //console.log(event);
// });

// document.addEventListener('keydown', function(event){
//     console.log("key down event");
//     //console.log(event);
// });

// !! when searching you might find the 'keypressed' event, please note that that one is being deprecated https://developer.mozilla.org/en-US/docs/Web/API/Element/keypress_event