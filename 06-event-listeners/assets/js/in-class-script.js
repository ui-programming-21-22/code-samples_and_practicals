console.log("hello from script");

function changeColour() {
    const rectBox = document.querySelector("#rect");
    let randomColor = Math.floor(Math.random()*16777215).toString(16);
    randomColor = "#" + randomColor;
    rectBox.style.backgroundColor = randomColor;
    let extraText = document.querySelector(".colourCode");
    extraText.innerHTML = randomColor;
}

const clickBox = document.querySelector(".clickBox");
clickBox.addEventListener("click", function(ev){
    console.log(ev);
    let innerClickBox = document.querySelector(".innerClickBox");
    innerClickBox.innerHTML = "click x: " + ev.x + "<br>" + "click y: " + ev.y + "<br>" + "alt key held: " + ev.altKey + "<br>" + "ctrl key held: " + ev.ctrlKey;
})

let character = document.querySelector(".character");
character.style.position = "relative";
character.style.left = "200px";
character.style.top = "200px";

document.addEventListener('keydown', input);

function input(event){
    if (event.type === "keydown"){
        switch (event.keyCode){
            case 37: // left arrow
                console.log("left arrow pressed");
                moveLeft();
                break;
            case 38: // up arrow
                console.log("up arrow pressed");
                moveUp();
                break;
            case 39: // right arrow
                console.log("right arrow pressed");
                moveRight();
                break;
            case 40: // down arrow
                console.log("down arrow pressed");
                moveDown();
                break;
            default:
                console.log("no input detected");
        }
    }
}

function moveLeft(){
    character.style.left = parseInt(character.style.left) - 5 + "px";
}

function moveUp(){
    character.style.top = parseInt(character.style.top) - 5 + "px";
}

function moveRight(){
    character.style.left = parseInt(character.style.left) + 5 + "px";
}

function moveDown(){
    character.style.top = parseInt(character.style.top) + 5 + "px";
}