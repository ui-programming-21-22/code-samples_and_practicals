let character = document.querySelector('.character');
character.style.position = "relative";
character.style.left = "200px";
character.style.top = "20px";

function input(event) {
    // Take Input from the Player
    // console.log("Input");
    // console.log("Event type: " + event.type);
    // console.log("Keycode: " + event.keyCode);

    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 37: // Left Arrow
                console.log("left arrow pressed");
                moveLeft();
                break; //Left key
            case 38: // Up Arrow
                console.log("up arrow pressed");
                moveUp();
                break; //Up key
            case 39: // Right Arrow
                console.log("right arrow pressed");
                moveRight();
                break; //Right key
            case 40: // Down Arrow
                console.log("down arrow pressed");
                moveDown();
                break; //Down key
            default:
               console.log("no movement key pressed")
        }
    }
    // console.log("Gamer Input :" + gamerInput.action);
}

window.addEventListener('keyup', input);
window.addEventListener('keydown', input);

function moveLeft(){
    character.style.left = parseInt(character.style.left) - 5 + "px";
}

function moveUp(){
    character.style.top = parseInt(character.style.top) - 5 + "px";
}

function moveRight(){
    character.style.left = parseInt(character.style.left) + 5 + "px";
}

function moveDown(){
    character.style.top = parseInt(character.style.top) + 5 + "px";
    
}
