# Gitlab Pages and `.gitlab-ci.yml`

The following process details how to extend one of your gitlab repositories into a hosting provider. Similar to Github, Gitlab allows you to (with some configuration needed) host your HTML, CSS and JS in a public website, with a specific URL.

The steps to follow are quite simple:

### 1. Create a `gitlab-ci.yml` file in the root of your repository

documentation link: https://docs.gitlab.com/ee/ci/quick_start/#create-a-gitlab-ciyml-file

All you need to do is to create a 'dot' file in the root of your repository, and give it the name: .gitlab-ci.yml

In this file, we will write (paste actually) some `yaml` code. Find out more about [YAML here](https://en.m.wikipedia.org/wiki/YAML)

The code to include in the .yml file is the following: 

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
    expire_in: 365 day
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

### 2. Create a 'public' folder in the root of your repo

Next you need to create a folder called 'public' in the root of your repository. In the `public` folder, you will put any files that you want gitlab to host for you. Make sure to include all you html, css, js, assets.

### 3. Commit your changes and watch the runner

* Add all these files to your commit and push them to the repo.
* Check the configuration of your repository and look for the General > Pages section. If the `.yml` file is configured correctly you will see a link to your gitlab page, it will look like this https://ui-programming-21-22.gitlab.io/code-samples_and_practicals
* You can also check the state of the runner, but this is optional.

## Practical 11:

Do all of this with your repository from practical 10, make it a public gitlab pages website and submit that link to the CA spreadsheet.

Add some text here.

Adding more text now on Tuesday AM


