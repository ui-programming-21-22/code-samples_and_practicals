# Web forms and data handeling in JS

## Task 1:
* Create a web page that contains a form. The form will ask the page visitor to input a name.
* Created a response to the user input with an alert, thanking them for their input
* Update the web page to include their name.

This is a type of validation feedback that informs the user that the form has been submitted successfully.

## Task 2:
* Add two other fields to the form, other types of information you might ask about a web user, handle the submitted data using JS and programmatically update the web page for it to include the submitted data.

## Deliverable: 
Create a gitlab.com repo that contains all your code, in the ui-programming-21-22 group, then submit a link to the repository in the CA submissions spreadsheet.

## Going beyond the practical

* try experimenting with the `required` attribute that can be added to `<input>` declarations.
* identify the different between `<input type="radio">` and `<input type="checkbox">`
* use a checkbox input and make sure you can extract and isolate the values that matter:
  ```
  <form action="/">
    <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike">
    <label for="vehicle1"> I have a bike</label><br>
    <input type="checkbox" id="vehicle2" name="vehicle2" value="Car">
    <label for="vehicle2"> I have a car</label><br>
    <input type="checkbox" id="vehicle3" name="vehicle3" value="Boat">
    <label for="vehicle3"> I have a boat</label><br><br>
    <input type="submit" value="Submit">
  </form>
  ```
