function GamerInput(input){
	this.action = input;
}

function input(event) {
    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Left key
            case 38: // Up Arrow
               	gamerInput = new GamerInput("Up");
                break; //Up key
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Right key
            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break; //Down key
            default:
            gamerInput = new GamerInput("None");
               
        }
    }
    else {
    	gamerInput = new GamerInput("None");
    }
}

function update(){
	if (gamerInput.action === "Up"){
		player.y -= 1;
	}
}



function gameloop(){
	update(); // to do
	draw(); // to do
	window.requestAnimationFrame(gameloop);	
}

window.requestAnimationFrame(gameloop);

window.addEventListener('keydown', input);
window.addEventListener('keyup', input);
