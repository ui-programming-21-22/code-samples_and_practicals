# animations and gameloops

### In this practical are required to:

* make use of the `Date()` object in Javascript, by making a timer in the corner of a html page that displays the current date, day of the week, month, year, hour, minute and second.

* style the clock in CSS by making it genuinely usable from a UI point of view: don't make the clock take over 100% of the screen, maybe positioning it in a corner of the screen would make more sense.

* use the GameLoop demonstrated in this chapter, by adding characters (images) to your canvas (using the configured method, or an augmented one created by you) 

* configure 3 more key-based eventListeners with keys other than the keyboard arrows and make one of your canvas characters react based on the input.
 
### during your practical demo you must demonstrate your ability to:

* use the `requestAnimationFrame()` method and explain your understanding of it 

* use the gameloop and detail its functionality, including any updates / changes you might have made to it
