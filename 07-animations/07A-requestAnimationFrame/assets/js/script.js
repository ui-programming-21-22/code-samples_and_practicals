let adiv = document.getElementById('mydiv');
    let leftpos = 0;

    function printLegible(timestamp){
        let date = new Date(timestamp);
        console.log("Date: "+date.getDate()+
            "/"+(date.getMonth()+1)+
            "/"+date.getFullYear()+
            " "+date.getHours()+
            ":"+date.getMinutes()+
            ":"+date.getSeconds());
    }
    function movediv(timestamp){
        //console.log(timestamp);
        printLegible(timestamp)
        leftpos += 0.2;
        adiv.style.left = leftpos + 'px';
        if (leftpos < 1200){
            window.requestAnimationFrame(movediv); // call requestAnimationFrame again to animate next frame
        }
    }
    window.requestAnimationFrame(movediv); // call requestAnimationFrame and pass into it animation function