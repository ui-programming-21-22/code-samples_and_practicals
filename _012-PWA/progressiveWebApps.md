# Progressive Web Apps

View the [Sample-PWA repository](https://gitlab.com/ui-programming-21-22/sample-pwa):

https://gitlab.com/ui-programming-21-22/sample-pwa

Building a PWA involves a few key steps.

It is best to start with a published git repository, and accessible html pages via gitlab-pages and the .gitlab-ci.yml mechanism.

## Adding a manifest.json

A manifest file declares the content of the app, what pages are important, some basic settings and the title(s) of your app.

You will need to make it using JSON, a notation file, the acronym of which stands for JavaScript Object Notation

Create a `manifest.json` file in the web-root of your project (meaning, if your web-pages are in a `public/` folder, you should save the manifest in that folder)

Take example on the sample files to see what to include in your manifest

https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json

## Adding a service-worker

A service worker is a special type of script that runs in the browser background. We already have regular js to interact with the front-end pages, service workers are meant to do background jobs. In our case it will manage what code pages we need to save for offline use.

The service worker will have to be declared using JS, so you can do that in your existing js files, or by creating a new one.

Take example on the sample code folder.

## Adding icons

As the main purpose of PWA, for us, is to make our game installable on mobile devices, it makes sense that we have to think about the launcher icons that we want the game to use. In [this folder](https://gitlab.com/ui-programming-21-22/sample-pwa/-/tree/main/public/imgs) you will see 4 transparent PNGS at different sizes to suit different devices. Make or use an existing icon, utilise these four sizes and declare them in your manifest.

## Checking your work

Two helpful ways of testing your progress when building a Progressive web app:

1. the 'Application' tab in your browser: it shows if your website has correctly declared a manifest and a service worker.

2. the lighthouse plugin: if you are using Chrome, lighthouse is just another tab in your inspector, if you're using another browser, you can install the Google Lighthouse plug-in which will generate a PWA report for you, indicating any errors that may have occured.

## Test and debug

When you have met all the requirements above, you should be able to install the app. Not all browsers will prompt you with an "install this app?" message, so check your browser menus.

## Module practical 12

For the practical of this chapter, a Lighthouse report will be ran on the url you submit, which will tell if you have successfully made a PWA. MAKE A NEW REPOSITORY, DO NOT REUSE YOUR REPO from Practial 11 for this practical.