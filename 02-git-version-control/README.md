When we use gitlab, github, bitbucket, gittea or any of the other online tools, at the core, we are using a program called git.

Git made by programmers to manage code and text files. It is meant specifically for collaboration and something called versionning.

![](https://gitlab.com/relearn/Relearn2013/-/blob/master/cheat-sheet/git-scheme.svg)

Make a gitlab.com account and flag the creation with Colm. You'll be added to the UI-Programming gitlab group.
